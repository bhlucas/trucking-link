const driver = require('../models').drivers;
const trailer = require('../models').trailers;
const vehicle = require('../models').vehicle;
const address = require('../models').address;
const drivingPreference = require('../models').drivingPreference;
const region = require('../models').regions;
const multer = require('multer');
var upload = multer({
    dest: '/tmp'
})

module.exports = {

    create(req, res) {
        upload.single('file');
        return driver
            .create({
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                middleName: req.body.middleName,
                dob: req.body.dob,
                bio: req.body.bio,
                trailer: {
                    van: req.body.trailer.van,
                    flatbed: req.body.trailer.flatbed,
                    refrigerated: req.body.trailer.refrigerated,
                    tanker: req.body.trailer.tanker,
                    specialized: req.body.trailer.specialized,
                    drayage: req.body.trailer.drayage,
                    other: req.body.trailer.other
                },
                region: {
                    northEast: req.body.regions.northEast,
                    midWest: req.body.regions.midWest,
                    southWest: req.body.regions.southWest,
                    southEast: req.body.regions.southEast,
                    plains: req.body.regions.plains,
                    westCoast: req.body.regions.westCoast
                },
                drivingPreference: {
                    local: req.body.drivingPreference.local,
                    regional: req.body.drivingPreference.regional,
                    road: req.body.drivingPreference.road,
                    hazmat: req.body.drivingPreference.hazmat,
                },
                vehicle: {
                    tractorTrailer: req.body.vehicle.tractorTrailer,
                    straightTruck: req.body.vehicle.straightTruck,
                    other: req.body.vehicle.other,
                },
                address: {
                    addressOne: req.body.address.addressOne,
                    addressTwo: req.body.address.addressOne,
                    city: req.body.address.city,
                    state: req.body.address.state,
                    postalCode: req.body.address.postalCode,
                }
            }, {
                include: [{
                    model: trailer
                }, {
                    model: region
                }, {
                    model: drivingPreference
                }, {
                    model: vehicle
                }, {
                    model: address
                }]
            })
            .then(driver => res.status(201).send(driver))
            .catch(error => res.status(400).send(error));
    },
    retrieve(req, res) {

        return driver
            .findById(req.params.driver_id, {
                include: [{
                    model: trailer
                }, {
                    model: region
                }, {
                    model: drivingPreference
                }, {
                    model: vehicle
                }, {
                    model: address
                }]
            })
            .then(driver => {
                if (!driver) {
                    return res.status(404).send({
                        message: 'Driver Not Found',
                    });
                }
                return res.status(200).send(driver);
            })
            .catch(error => res.status(400).send(error));
    },
};
