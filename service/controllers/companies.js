const Company = require('../models').company;
const Trailer = require('../models').trailers;
const Contact = require('../models').contact;
const Address = require('../models').address;
const Corperation = require('../models').corperation;
const Regions = require('../models').regions;
const multer = require('multer');
var upload = multer({
    dest: '/tmp'
})

module.exports = {
    create(req, res) {
        return Company.create({
                name: req.body.name,
                carrierName: req.body.carrierName,
                scacCode: req.body.scacCode,
                dotNumber: req.body.scacCode,
                mcNumber: req.body.mcNumber,
                terminalLocations: req.body.terminalLocations,
                regionsServiced: req.body.regionsServiced,
                website: req.body.website,
                yearsInBusiness: req.body.yearsInBusiness,
                satelliteTracking: req.body.satelliteTracking,
                electronicLogs: req.body.electronicLogs,
                companyPowerUnits: req.body.companyPowerUnits,
                operatorPowerUnits: req.body.operatorPowerUnits,
                milesPerDay: req.body.milesPerDay,
                trailer: {
                    van: req.body.trailer.van,
                    flatbed: req.body.trailer.flatbed,
                    refrigerated: req.body.trailer.refrigerated,
                    tanker: req.body.trailer.tanker,
                    specialized: req.body.trailer.specialized,
                    drayage: req.body.trailer.drayage,
                },
                regions: {
                    northEast: req.body.regions.northEast,
                    midWest: req.body.regions.midWest,
                    southWest: req.body.regions.southWest,
                    southEast: req.body.regions.southEast,
                    plains: req.body.regions.plains,
                    westCoast: req.body.regions.westCoast
                },
                corperation: {
                    phone: req.body.corperation.phone,
                    address: {
                        addressOne: req.body.corperation.address.addressOne,
                        addressTwo: req.body.corperation.address.addressOne,
                        city: req.body.corperation.address.city,
                        state: req.body.corperation.address.state,
                        postalCode: req.body.corperation.address.postalCode,
                    }
                },
                contact: {
                    name: req.body.contact.name,
                    title: req.body.contact.title,
                    email: req.body.contact.email,
                    phone: req.body.contact.phone,
                    address: {
                        addressOne: req.body.contact.address.addressOne,
                        addressTwo: req.body.contact.address.addressOne,
                        city: req.body.contact.address.city,
                        state: req.body.contact.address.state,
                        postalCode: req.body.contact.address.postalCode,
                    }
                },

            }, {
                include: [{
                    model: Trailer
                }, {
                    model: Regions
                }, {
                    model: Contact,
                    include: [{
                        model: Address
                    }]
                }, {
                    model: Corperation,
                    include: [{
                        model: Address
                    }]
                }]
            })
            .then(Company => res.status(201).send(Company))
            .catch(error => res.status(400).send(error));
    },
    retrieve(req, res) {
        return Company
            .findById(req.params.company_id, {
                include: [{
                    model: Trailer
                }, {
                    model: Regions
                }, {
                    model: Contact,
                    include: [{
                        model: Address
                    }]
                }, {
                    model: Corperation,
                    include: [{
                        model: Address
                    }]
                }]
            })
            .then(Company => {
                if (!Company) {
                    return res.status(404).send({
                        message: 'Company Not Found',
                    });
                }
                return res.status(200).send(Company);
            })
            .catch(error => res.status(400).send(error));
    },
    update(req, res) {
        console.log("company_id: " + req.params.company_id);
        return Company
            .findById(req.params.company_id, {
                include: [{
                    model: Trailer
                }, {
                    model: Regions
                }, {
                    model: Contact,
                    include: [{
                        model: Address
                    }]
                }, {
                    model: Corperation,
                    include: [{
                        model: Address
                    }]
                }]
            })
            .then(company => {
                if (!company) {
                    return res.status(404).send({
                        message: 'Todo Not Found',
                    });
                }
                return company.update({
                        name: req.body.name,
                        carrierName: req.body.carrierName,
                        scacCode: req.body.scacCode,
                        dotNumber: req.body.dotNumber,
                        mcNumber: req.body.mcNumber,
                        terminalLocations: req.body.terminalLocations,
                        regionsServiced: req.body.regionsServiced,
                        website: req.body.website,
                        yearsInBusiness: req.body.yearsInBusiness,
                        satelliteTracking: req.body.satelliteTracking,
                        electronicLogs: req.body.electronicLogs,
                        companyPowerUnits: req.body.companyPowerUnits,
                        operatorPowerUnits: req.body.operatorPowerUnits,
                        milesPerDay: req.body.milesPerDay,

                        regions: {
                            northEast: req.body.regions.northEast,
                            midWest: req.body.regions.midWest,
                            southWest: req.body.regions.southWest,
                            southEast: req.body.regions.southEast,
                            plains: req.body.regions.plains,
                            westCoast: req.body.regions.westCoast
                        },
                        corperation: {
                            phone: req.body.corperation.phone,
                            address: {
                                addressOne: req.body.corperation.address.addressOne,
                                addressTwo: req.body.corperation.address.addressOne,
                                city: req.body.corperation.address.city,
                                state: req.body.corperation.address.state,
                                postalCode: req.body.corperation.address.postalCode,
                            }
                        },
                        contact: {
                            name: req.body.contact.name,
                            title: req.body.contact.title,
                            email: req.body.contact.email,
                            phone: req.body.contact.phone,
                            address: {
                                addressOne: req.body.contact.address.addressOne,
                                addressTwo: req.body.contact.address.addressOne,
                                city: req.body.contact.address.city,
                                state: req.body.contact.address.state,
                                postalCode: req.body.contact.address.postalCode,
                            }
                        },
                    })
                    .then(company => {
                        company.Trailer.update({
                                van: req.body.trailer.van,
                                flatbed: req.body.trailer.flatbed,
                                refrigerated: req.body.trailer.refrigerated,
                                tanker: req.body.trailer.tanker,
                                specialized: req.body.trailer.specialized,
                                drayage: req.body.trailer.drayage,
                            })
                            .then(() => console.log("updated Trailer")) // Send back the updated todo.
                            .catch((error) => console.log("error: " + error));
                    }) // Send back the updated todo.
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));
    },
};
