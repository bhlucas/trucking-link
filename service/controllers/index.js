const drivers = require('./drivers');
const companies = require('./companies');

module.exports = {
  drivers,
  companies,
};
