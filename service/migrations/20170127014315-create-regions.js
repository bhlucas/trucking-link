'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('regions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      north - east: {
        type: Sequelize.BOOLEAN
      },
      mid - west: {
        type: Sequelize.BOOLEAN
      },
      south - east: {
        type: Sequelize.BOOLEAN
      },
      south - west: {
        type: Sequelize.BOOLEAN
      },
      plains: {
        type: Sequelize.BOOLEAN
      },
      west - coast: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('regions');
  }
};