'use strict';
module.exports = function(sequelize, DataTypes) {
    var company = sequelize.define('company', {

      name: { type: DataTypes.STRING, field: "name" },
      carrierName : { type: DataTypes.STRING, field: "carrier_name" },
      scacCode: { type: DataTypes.STRING, field: "scac_code" },
      dotNumber: { type: DataTypes.STRING, field: "dot_number" },
      mcNumber: { type: DataTypes.STRING, field: "mc_number" },
      terminalLocations: { type: DataTypes.STRING, field: "terminal_locations" },
      regionsServiced: { type: DataTypes.STRING, field: "regions_serviced" },
      website: { type: DataTypes.STRING, field: "website" },
      yearsInBusiness: { type: DataTypes.STRING, field: "years_in_business" },
      satelliteTracking: { type: DataTypes.BOOLEAN, field: "satellite_tracking" },
      electronicLogs: { type: DataTypes.BOOLEAN, field: "electronic_logs" },
      companyPowerUnits: { type: DataTypes.INTEGER, field: "company_power_units" },
      operatorPowerUnits: { type: DataTypes.INTEGER, field: "operator_power_units" },
      milesPerDay: { type: DataTypes.INTEGER, field: "miles_per_day" },
    }, {
        classMethods: {
            associate: function(models) {
                company.hasOne(models.trailers, {
                  foreignKey: "driver"
                });
                company.hasOne(models.corperation, {
                  foreignKey: "driver"
                });
                company.hasOne(models.contact, {
                  foreignKey: "driver"
                });
                company.hasOne(models.regions, {
                  foreignKey: "driver"
                });
            }
        }
    });
    return company;
};
