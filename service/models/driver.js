'use strict';
module.exports = function(sequelize, DataTypes) {
    var drivers = sequelize.define('drivers', {
        firstName: { type: DataTypes.STRING, field: "first_name" },
        lastName: { type: DataTypes.STRING, field: "last_name" },
        middleName: { type: DataTypes.STRING, field: "middle_name" },
        yearsExperience: { type: DataTypes.STRING, field: "years_experience" },
        milesDriven: { type: DataTypes.STRING, field: "miles_driven" },
        twicCard: DataTypes.BOOLEAN,
        dob: DataTypes.STRING,
        bio: DataTypes.STRING,
    }, {
        classMethods: {
            associate: function(models) {
                drivers.hasOne(models.trailers, {
                  foreignKey: "driver"
                });
                drivers.hasOne(models.regions, {
                  foreignKey: "driver"
                });
                drivers.hasOne(models.drivingPreference, {
                  foreignKey: "driver"
                });
                drivers.hasOne(models.vehicle, {
                  foreignKey: "driver"
                });
                drivers.hasOne(models.address, {
                  foreignKey: "driver"
                });
            }
        }
    });
    return drivers;
};
