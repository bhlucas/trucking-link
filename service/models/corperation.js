'use strict';
module.exports = function(sequelize, DataTypes) {
    var corperation = sequelize.define('corperation', {
      phone: { type: DataTypes.STRING, field: "phone" },
    }, {
        classMethods: {
            associate: function(models) {
                corperation.hasOne(models.address, {
                  foreignKey: "driver"
                });
            }
        }
    });
    return corperation;
};
