'use strict';
module.exports = function(sequelize, DataTypes) {
    var drivingPreference = sequelize.define('drivingPreference', {
        local: DataTypes.BOOLEAN,
        regional: DataTypes.BOOLEAN,
        road: DataTypes.BOOLEAN,
        hazmat: DataTypes.BOOLEAN,
        driver: {
            type: DataTypes.INTEGER,
            allowNull: true,
            field: 'driver',
            references: {
                model: 'drivers',
                key: 'id'
            }
        }
    }, {
        classMethods: {
            //associate: function(models) {
            //    trailers.belongsTo(models.drivers);
            //}
        },
        tableName: 'driving_preference'
    });
    return drivingPreference;
};
