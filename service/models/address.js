'use strict';
module.exports = function(sequelize, DataTypes) {
  var address = sequelize.define('address', {
    addressOne: DataTypes.STRING,
    addressTwo: DataTypes.STRING,
    city: DataTypes.STRING,
    state: DataTypes.STRING,
    postalCode: DataTypes.STRING,
    driver: {
        type: DataTypes.INTEGER,
        allowNull: true,
        field: 'driver',
        references: {
            model: 'drivers',
            key: 'id'
        }
    }
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return address;
};
