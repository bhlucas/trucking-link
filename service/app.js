const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const models = require('./models');
const http = require('http');


// Set up the express app
const app = express();

// Log requests to the console.
app.use(logger('dev'));

app.use(function(req, res, next) {
  res.header(  "Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// Parse incoming requests data (https://github.com/expressjs/body-parser)
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

require('./routes')(app);
// Setup a default catch-all route that sends back a welcome message in JSON format.
app.get('*', (req, res) => res.status(200).send({
  message: 'Welcome to the beginning of nothingness.',
}));

var port = process.env.PORT || 3001;

models.sequelize.sync().then(function () {

  http.createServer(app).listen(port, function (err) {
    console.log('listening in http://localhost:' + port);
  });
});

module.exports = app;
