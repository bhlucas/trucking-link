// This will be our application entry. We'll setup our server here.
const http = require('http');
const app = require('../app'); // The express app we just created
const models = require('../models');

const port = parseInt(process.env.PORT, 10) || 8000;
app.set('port', port);

models.sequelize.sync({force:true}).then(function () {
	console.log("sequelize sync running.");
  var server = app.listen(app.get('port'), function() {
    debug('Express server listening on port ' + server.address().port);
  });
});

//const server = http.createServer(app);
//server.listen(port);
