import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('company/driving-preference', 'Integration | Component | company/driving preference', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{company/driving-preference}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#company/driving-preference}}
      template block text
    {{/company/driving-preference}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
