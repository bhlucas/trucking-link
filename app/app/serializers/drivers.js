import DS from 'ember-data';

export default DS.JSONSerializer.extend(DS.EmbeddedRecordsMixin, {
  attrs: {
   trailer: { embedded: 'always' },
   regions: { embedded: 'always' }
 }
});
