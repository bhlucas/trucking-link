import DS from 'ember-data';

export default DS.JSONSerializer.extend(DS.EmbeddedRecordsMixin, {
  attrs: {
   trailer: { embedded: 'always'},
   regions: { embedded: 'always'},
   vehicle: { embedded: 'always'},
   address: { embedded: 'always'},
   corperation: { embedded: 'always'},
   contact: { embedded: 'always'},
   drivingPreference: { embedded: 'always'}
 }
});
