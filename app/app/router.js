import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('driver', function() {
    this.route('step1');
    this.route('step2');

    this.route('register', function() {
      this.route('step1');
      this.route('step2');
    });
  });
  this.route('landing', function() {
    this.route('register');
    this.route('login');
  });
  this.route('register', function() {
    this.route('step1');
    this.route('step2');
  });

  this.route('company', function() {
    this.route('step1', { path: '/step1'});
    this.route('step1', { path: '/step1/:company_id'});
    this.route('step2', { path: '/step2/:company_id'});
    this.route('step2', { path: '/step2'});
    this.route('step3', { path: '/step3/:company_id'});
    this.route('step3', { path: '/step3'});
    this.route('profile', { path: '/profile/:company_id'});
    this.route('edit', { path: '/edit/:company_id'});

    this.route('register', function() {
      this.route('step1');
      this.route('step2');
      this.route('step3');
    });
  });
  this.route('search');

  this.route('conpany', function() {});
});

export default Router;
