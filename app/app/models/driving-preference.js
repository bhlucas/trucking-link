import DS from 'ember-data';

export default DS.Model.extend({
  local: DS.attr('boolean', { defaultValue: false }),
  regional: DS.attr('boolean', { defaultValue: false }),
  road: DS.attr('boolean', { defaultValue: false }),
  hazmat: DS.attr('boolean', { defaultValue: false })
});
