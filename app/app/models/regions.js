import DS from 'ember-data';

export default DS.Model.extend({
  northEast: DS.attr('boolean', { defaultValue: false }),
  midWest: DS.attr('boolean', { defaultValue: false }),
  southEast: DS.attr('boolean', { defaultValue: false }),
  southWest: DS.attr('boolean', { defaultValue: false }),
  plains: DS.attr('boolean', { defaultValue: false }),
  westCoast: DS.attr('boolean', { defaultValue: false })
});
