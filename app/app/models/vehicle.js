import DS from 'ember-data';

export default DS.Model.extend({
  tractorTrailer: DS.attr('boolean', { defaultValue: false }),
  straightTruck:DS.attr('boolean', { defaultValue: false }),
  other: DS.attr('string')
});
