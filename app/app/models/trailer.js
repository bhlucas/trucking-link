import DS from 'ember-data';

export default DS.Model.extend({
  dryVan: DS.attr('boolean', { defaultValue: false }),
  flatbed: DS.attr('boolean', { defaultValue: false }),
  refrigerated: DS.attr('boolean', { defaultValue: false }),
  tanker: DS.attr('boolean', { defaultValue: false }),
  tankerEndorced: DS.attr('boolean', { defaultValue: false }),
  specialized: DS.attr('boolean', { defaultValue: false }),
  drayage: DS.attr('boolean', { defaultValue: false }),
  other: DS.attr('string')
});
