import DS from 'ember-data';

export default DS.Model.extend({
  addressOne: DS.attr('string'),
  addressTwo: DS.attr('string'),
  city: DS.attr('string'),
  state: DS.attr('string'),
  postalCode: DS.attr('string'),
});
