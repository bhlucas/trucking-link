import Ember from 'ember';

export default Ember.Component.extend({
  // didInsertElement() {
  //   this._super(...arguments);
  //   Ember.$('.ui.form')
  //     .form({
  //       fields: {
  //         name: {
  //           identifier: 'name',
  //           rules: [{
  //             type: 'empty',
  //             prompt: 'Please enter your name'
  //           }]
  //         },
  //       }
  //     });
  // },
  didRender() {
    Ember.$('.ui.form')
      .form({
        on: 'blur',
        onSuccess: function(event) {
          // debugger;
          //event.preventDefault()
          // Ember.$('.submit').removeClass('disabled');
          return true;
        },
        onFailure: function(event) {
          // debugger;
          //event.preventDefault()
          // Ember.$('.submit').addClass('disabled');
          return false;
        },
        fields: {
          name: {
            identifier: 'name',
            rules: [{
              type: 'empty',
              prompt: 'Please enter your name'
            }]
          },
          title: {
            identifier: 'title',
            rules: [{
              type: 'empty',
              prompt: 'Please enter your title'
            }]
          },
          email: {
            identifier: 'email',
            rules: [{
              type: 'email',
              prompt: 'Please enter a valid e-mail'
            }]
          },
          phone: {
            identifier: 'phone',
            rules: [{
              type: 'regExp[/^(\\d{3})\-(\\d{3})\-(\\d{4})$/]',
              prompt: 'Please enter a valid phone number (xxx-xxx-xxxx)'
            }]
          },
          addressOne: {
            identifier: 'addressOne',
            rules: [{
              type: 'empty',
              prompt: 'Please enter your address'
            }]
          },
          city: {
            identifier: 'city',
            rules: [{
              type: 'empty',
              prompt: 'Please enter your city'
            }]
          },
          state: {
            identifier: 'state',
            rules: [{
              type: 'empty',
              prompt: 'Please enter your state'
            }]
          },
          postalCode: {
            identifier: 'postalCode',
            rules: [{
              type: 'empty',
              prompt: 'Please enter your postal code'
            }]
          },
        }
      });
  }
});
