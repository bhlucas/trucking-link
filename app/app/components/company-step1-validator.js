import Ember from 'ember';

export default Ember.Component.extend({
  // didInsertElement() {
  //   this._super(...arguments);
  //   Ember.$('.ui.form')
  //     .form({
  //       fields: {
  //         name: {
  //           identifier: 'name',
  //           rules: [{
  //             type: 'empty',
  //             prompt: 'Please enter your name'
  //           }]
  //         },
  //       }
  //     });
  // },
  didRender() {
    Ember.$('.ui.form')
      .form({
        on: 'blur',
        onSuccess: function(event) {
          // debugger;
          //event.preventDefault()
          // Ember.$('.submit').removeClass('disabled');
          return true;
        },
        onFailure: function(event) {
          // debugger;
          //event.preventDefault()
          // Ember.$('.submit').addClass('disabled');
          return false;
        },
        fields: {
          name: {
            identifier: 'name',
            rules: [{
              type: 'empty',
              prompt: 'Please enter your name'
            }]
          },
          carrierName: {
            identifier: 'carrierName',
            rules: [{
                type: 'empty',
                prompt: 'Please enter your carrier name'
            }]
          },
          website: {
            identifier: 'website',
            rules: [{
              type   : 'url',
              prompt: 'Please enter a valid web address'
            }]
          },
          yearsInBusiness: {
            identifier: 'yearsInBusiness',
            rules: [{
              type   : 'integer[1..100]',
              prompt: 'Please enter the your number of years in business'
            }]
          },
          companyPowerUnits: {
            identifier: 'companyPowerUnits',
            rules: [{
              type   : 'integer[1..100]',
              prompt: 'Please enter your the number of company power units'
            }]
          },
          operatorPowerUnits: {
            identifier: 'operatorPowerUnits',
            rules: [{
              type   : 'integer[1..100]',
              prompt: 'Please enter your the number of operator power units'
            }]
          },
          scacCode: {
            identifier: 'scacCode',
            rules: [{
              type: 'empty',
              prompt: 'Please enter your SCAC code'
            }]
          },
          dotNumber: {
            identifier: 'dotNumber',
            rules: [{
              type: 'empty',
              prompt: 'Please enter your DOT number'
            }]
          },
          mcNumber: {
            identifier: 'mcNumber',
            rules: [{
              type: 'empty',
              prompt: 'Please enter your MC number'
            }]
          },
          milesPerDay: {
            identifier: 'milesPerDay',
            rules: [{
              type: 'empty',
              prompt: 'Please enter your standard solo miles per day'
            }]
          },
        }
      });
  }
});
