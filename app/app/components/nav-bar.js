import Ember from 'ember';
import Base from 'semantic-ui-ember/mixins/base';

export default Ember.Component.extend({
  didInsertElement: function() {
         Ember.run.scheduleOnce('afterRender', this, function() {
           Ember.$('.ui .dropdown').dropdown();
         });
     }
});
