import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return this.modelFor('driver');
  },
  actions: {
    register() {
      debugger;
      this.modelFor('driver').save();
      this.transitionTo('driver');
    },
    previous() {
      this.transitionTo('driver.step1');
    }
  }
});
