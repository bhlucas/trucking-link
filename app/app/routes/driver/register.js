import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return this.store.createRecord('driver', {
      regions: this.store.createRecord('regions'),
      trailer: this.store.createRecord('trailer'),
      vehicle: this.store.createRecord('vehicle'),
      address: this.store.createRecord('address'),
      drivingPreference: this.store.createRecord('driving-preference')
    });
  },
});
