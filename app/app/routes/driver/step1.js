import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return this.modelFor('driver');
  },
  actions: {
    selectState(){
      debugger;
      this.get('model').set('state', state);
    },
    nextStep() {
      this.transitionTo('driver.step2');
    },
    cancel() {
      this.transitionTo('landing.register');
    }
  }
});
