import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return this.modelFor('company.register');
  },
  actions: {
    selectState() {
      this.get('model').set('state', state);
    },
    nextStep() {
      this.transitionTo('company.register.step3');
    },
    previous() {
      this.transitionTo('company.register.step1');
    }
  }
});
