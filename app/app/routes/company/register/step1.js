import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return this.modelFor('company.register');
  },
  actions: {
    loading(transition, originRoute) {
      debugger;
    },
    nextStep() {
      debugger;
      var params = this.get('params');
      if (params) {
        this.transitionTo('company.register.step2', params.company_id);
      } else {
        this.transitionTo('company.register.step2');
      }
    },
    cancel() {
      this.transitionTo('landing.register');
    }
  }
});
