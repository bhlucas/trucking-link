import Ember from 'ember';

export default Ember.Route.extend({
  model(params) {
    debugger;
    this.set('params', params);
    var model = this.currentModel;
    if(model){
      return model;
    }else{
      debugger;
      var company = this.store.findRecord('company', params.company_id);
      return company;
    }
  },
  actions: {
    edit(model) {
      var params = this.get('params');
      debugger;
      var company = this.currentModel;
      this.transitionTo('company.edit', params.company_id);
    }
  }
});
