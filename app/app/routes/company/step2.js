import Ember from 'ember';

export default Ember.Route.extend({
  model(params) {
    debugger;
    let company = null;
    if (params.company_id) {
       company = this.store.peekRecord('company', params.company_id);
    }
    if(company){
      return company;
    } else {
      return this.modelFor('company');
    }
  },
  actions: {
    selectState(){
      this.get('model').set('state', state);
    },
    nextStep() {
      var params = this.get('params');
      if(params){
        this.transitionTo('company.step3', params.company_id);
      } else {
        this.transitionTo('company.step3');

      }
    },
    previous() {
      this.transitionTo('company.step1');
    }
  }
});
