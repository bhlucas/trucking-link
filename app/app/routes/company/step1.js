import Ember from 'ember';

export default Ember.Route.extend({

  model(params) {
    debugger;
    let company = null;
    if (params.company_id) {
       company = this.store.peekRecord('company', params.company_id);
    }
    if(company){
      return company;
    } else {
      return this.modelFor('company');
    }
  },
  actions: {
    loading(transition, originRoute) {
      debugger;
    },
    nextStep() {
      debugger;
      var params = this.get('params');
      if(params){
        this.transitionTo('company.step2', params.company_id);
      } else {
        this.transitionTo('company.step2');
      }
    },
    cancel() {
      this.transitionTo('landing.register');
    }
  }
});
