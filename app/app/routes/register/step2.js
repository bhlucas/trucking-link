import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return this.modelFor('register');
  },
  actions: {
    register() {
      debugger;
      this.modelFor('register').save();
      this.transitionTo('search');
    }
  }
});
